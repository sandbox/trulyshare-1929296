
TrulyShare ShopOnPage Module provides a simple solution for affiliate advertising
on your website. 


Installation
------------

Extract the package to your modules directory.
Go to the configuration form for the module.
Enter your publisher id into the form. If you do not have an ID, visit http://www.trulyshare.com/publishers/new


Author
------
TrulyShare
contact@trulyshare.com
